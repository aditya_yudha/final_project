@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card border-secondary mb-3" style="max-width: 100%;">
    <div class="card-header">Header</div>
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Type</th>
                    <th scope="col">Column heading</th>
                    <th scope="col">Column heading</th>
                    <th scope="col">Column heading</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <th scope="row">Default</th>
                <td>Column content</td>
                <td>Column content</td>
                <td>Column content</td>
                </tr>
                </tbody>
        </table>
    </div>
    </div>
</div>
@endsection
